from pathlib import Path

fileSource = 'strings.txt'
filePath = Path(fileSource)
if filePath.is_file():
	file = open(fileSource)
	line = file.readline()
	lineParts = line.split(' ')
	print(len(lineParts))
	file.close()
else:
	print('File %s not found!' % fileSource)